import React from 'react';
import './App.css';

const App = () => {

  const sayHello = () => {
    alert('Hello World!');
  }

  return (
    <div className="container">
    <div className="row justify-content-center">
      <h1>KUSH React</h1>
    </div>
    <div className="row justify-content-center">
      <div className="col-xs-2">
        <button className='btn btn-primary' onClick={ sayHello }>Hello World</button>
      </div>
    </div>
    <div className="row justify-content-center">
      <div className="col-xs-2">
        <i className='fa fa-coffee fa-7x'></i>
      </div>
    </div>
  </div>
  );
}

export default App;